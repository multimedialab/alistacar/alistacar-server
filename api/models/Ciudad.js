/* Ciudad.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {
  
  autoPK: false,
  
  autoCreatedAt: false,
  
  autoUpdatedAt: false,

  attributes: {
    // Keys
    id_ciudad: {
      type: "integer",
      primaryKey: true,
      autoIncrement: true,
      size: 11
    },
    puntos: {
      collection: 'punto_de_servicio',
      via: 'id_ciudad'
    },
    // Attributes
    nombre: {
      type: "string",
      size: 45
    }
  }
};