/**
 * Servicio.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/#!documentation/models
 */

module.exports = {

    autoPK: false,

    autoCreatedAt: false,

    autoUpdatedAt: false,

    attributes: {
        // Keys
        id_servicio: {
            type: "integer",
            primaryKey: true,
            autoIncrement: true,
            size: 11
        },
        id_punto_de_servicio: {
            model: 'punto_de_servicio'
        },
        // Models
        id_sub_categoria: {
            type: "integer",
            size: 11,
            defaultsTo: null
        },
        nombre: {
            type: "string",
            size: 255
        },
        imagen: {
            type: "string",
            defaultsTo: null,
            required: true,
            size: 300
        },
        posicion: {
            type: "integer",
            size: 11,
            defaultsTo: 0
        },
        precio: {
            type: 'float',
            defaultsTo: null
        },
        descuento: {
            type: 'integer',
            defaultsTo: null
        },
        descripcion: {
            type: "string",
            defaultsTo: null,
            required: true,
            size: 190
        },
        tipo: {
            type: "string",
            size: 45
        }
    }
};