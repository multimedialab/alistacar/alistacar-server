/**
 * Usuario.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

    autoPK: false,

    autoCreatedAt: true,

    autoUpdatedAt: false,

    attributes: {
        id_usuario: {
            type: "integer",
            primaryKey: true,
            autoIncrement: true,
            size: 11
        },
        nombre: {
            type: "string",
            size: 255
        },
        email: {
            type: "string",
            size: 255
        },
        telefono: {
            type: "string",
            size: 255
        },
        contrasena: {
            type: "string",
            size: 255
        },
        dispositivo: {
            type: "string",
            size: 45
        },
        token: {
            type: "string",
            size: 255
        },
        created: {
            type: "string",
            size: 45
        }
    }
};