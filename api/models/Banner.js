/**
 * Banner.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {
  
  autoPK: false,
  
  autoCreatedAt: false,
  
  autoUpdatedAt: false,
  
  attributes: {
    id_banner: {
      type: "integer",
      primaryKey: true,
      autoIncrement: true
    },
    imagen: {
      type: "string",
      size: 300
    },
    nombre: {
      type: "string",
      size: 45
    },
    id_punto_de_servicio: {
      type: "integer"
    }
  }
};

