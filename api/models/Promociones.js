/**
 * Promociones.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

    autoPK: false,

    autoCreatedAt: false,

    autoUpdatedAt: false,

    attributes: {
        id_promocion: {
            type: "integer",
            primaryKey: true,
            autoIncrement: true
        },
        id_punto_de_servicio: {
            type: "integer"
        },
        imagen: {
            type: "string",
            size: 255
        }
    }
};