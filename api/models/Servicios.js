/**
 * Servicios.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

    autoPK: false,

    autoCreatedAt: true,

    autoUpdatedAt: true,

    attributes: {
        id_servicio: {
            type: "integer",
            primaryKey: true,
            autoIncrement: true,
            size: 11
        },
        id_punto_de_servicio: {
            type: "integer",
            size: 11
        },
        nombre_punto_de_servicio: {
            type: "string",
            size: 255
        },
        ciudad: {
            type: "string",
            size: 255
        },
        id_usuario: {
            type: "integer",
            size: 11
        },
        id_empresa: {
            type: "integer",
            size: 11
        },
        nombre_servicio: {
            type: "string",
            size: 255
        },
        precio: {
            type: "string",
            size: 45
        },
        fecha: {
            type: "datetime"
        },
        nombre_cliente: {
            type: "string",
            size: 255
        },
        telefono_cliente: {
            type: "string",
            size: 255
        },
        estado: {
            type: "boolean"
        },
        terminado: {
            type: "boolean"
        },
        calificado: {
            type: "boolean"
        },
        status: {
            type: "string",
            size: 45
        },
        revisado: {
            type: "boolean"
        }
    }
};