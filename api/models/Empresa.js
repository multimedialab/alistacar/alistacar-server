/**
 * Empresa.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  autoPK: false,
  
  autoCreatedAt: false,
  
  autoUpdatedAt: false,

  attributes: {
    id_empresa: {
      type: "integer",
      primaryKey: true,
      autoIncrement: true,
      size: 11
    },
    nombre: {
      type: "string",
      size: 255
    },
    dispositivos: {
      type: "json"
    }
  }
};

