/**
 * Punto_de_servicio.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/#!documentation/models
 */

module.exports = {
  
  autoPK: false,
  
  autoCreatedAt: false,
  
  autoUpdatedAt: false,
  
  attributes: {
    //Models
    id_punto_de_servicio: {
      type: "integer",
      primaryKey: true,
      autoIncrement: true,
      size: 11
    },
    id_empresa: {
      type: "integer"
    },
    id_ciudad: {
      model: 'ciudad'
    },
    servicios: {
      collection: 'servicio',
      via: 'id_punto_de_servicio'
    },
    comentarios: {
      collection: 'comentarios',
      via: 'id_punto_de_servicio'
    },
    //Attributes
    nombre: {
      type: "string",
      size: 255
    },
    slogan: {
      type: "string",
      size: 255
    },
    direccion: {
      type: "string",
      size: 100
    },
    imagen: {
      type: "string",
      defaultsTo: null,
      required: true,
      size: 255
    },
    medio_pago: {
      type: "string",
      defaultsTo: null,
      required: true,
      size: 45
    },
    latitud: {
      type: "float",
      defaultsTo: null,
      required: true
    },
    longitud: {
      type: "float",
      defaultsTo: null,
      required: true
    },
    horario: {
      type: "string",
      defaultsTo: null,
      required: true
    },
    coordenadas: {
      type: "string",
      defaultsTo: null,
      required: true,
      size: 300
    },
    nit_o_rut: {
      type: "string",
      size: 100
    },
    telefono: {
      type: "string",
      defaultsTo: null,
      required: true,
      size: 45
    },
    email: {
      type: "string",
      defaultsTo: null,
      required: true,
      size: 100
    },
    dispositivos: {
      type: "json",
      defaultsTo: null
    },
    contrasenia: {
      type: "string",
      defaultsTo: null
    },
    es_master: {
      type: "boolean"
    }
  }
};