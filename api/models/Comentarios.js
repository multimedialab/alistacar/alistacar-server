/**
 * Comentarios.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {
  
  autoPK: false,
  
  autoCreatedAt: false,
  
  autoUpdatedAt: false,

  attributes: {
    //keys
    id_comentarios: {
      type: "integer",
      primaryKey: true,
      autoIncrement: true,
      size: 11
    },
    id_punto_de_servicio: {
      model: 'punto_de_servicio'
    },
    //Attributes
    contenido: {
      type: "string",
      size: 255
    },
    calificacion: {
      type: "integer"
    }
  }
};

