/**
* Categoria.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {
  
  autoPK: false,
  
  autoCreatedAt: false,
  
  autoUpdatedAt: false,

  attributes: {
    // Keys
    id_categoria: {
      type: "integer",
      primaryKey: true,
      autoIncrement: true,
      size: 11
    },
    id_categoria_padre: {
      model: "categoria"
    },
    
    // Attributes
    nombre: {
      type: "string",
      size: 100
    },
    tipo_servicio: {
      type: "string",
      size: 100
    },
    imagen: {
      type: "string",
      size: 300
    },
    imagen_fondo: {
      type: "string",
      size: 300
    },
    posicion: {
      type: "integer"
    },
    padre: {
      collection: 'categoria',
      via: 'id_categoria_padre'
    }
  }
};