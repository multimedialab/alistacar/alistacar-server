/**
 * PartnerController
 *
 * @description :: Server-side logic for managing Partners
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

var request = require('request');
var path = require('path');
var fs = require('fs');
var moment = require('moment');
var EmailTemplate = require('email-templates').EmailTemplate;
moment.locale('es');


//service.on("connected", function() { console.log("Connected"); }); service.on("transmitted", function(notification, device) { console.log("Notification transmitted to:" + device.token.toString("hex")); }); service.on("transmissionError", function(errCode, notification, device) { console.error("Notification caused error: " + errCode); }); service.on("timeout", function () { console.log("Connection Timeout"); }); service.on("disconnected", function() { console.log("Disconnected from APNS"); }); service.on("socketError", console.error);
module.exports = {

    pushtoall: async function (req, res) {
        var body = req.query;
        try {
            var empresa = await Punto_de_servicio.find();

            var dispositivos = [];
            for (var mKey in empresa) {
                if (empresa[mKey].dispositivos) {
                    dispositivos.push(empresa[mKey].dispositivos);
                }
            }
            var titulo = body.title;
            var mensaje = body.content;
            var payload = null;
            var tipo_usuario = "partner";

            return res.send(PushService.send_push(dispositivos, titulo, mensaje, payload, tipo_usuario));
        } catch (e) {
            return res.serverError(e);
        }

    },
    // valida la contrasena y el usuario si son correctos
    login: function (req, res) {
        if (!req.isSocket)
            return res.badRequest();
        Punto_de_servicio.findOne({ nit_o_rut: req.param('usuario'), contrasenia: req.param('password') }).exec(function (error, punto) {
            if (error) {
                res.negotiate(error); console.log("el error es", error);
                return false;
            }

            res.json(punto);
            return false;
        });
    },


    servicios: function (req, res) {
        if (!req.isSocket)
            return res.badRequest();

        Punto_de_servicio.findOne({ id_punto_de_servicio: req.param('id') }).exec(function (error, punto) {
            if (error || !punto) {
                res.negotiate();
                return false;
            }

            if (punto.es_master) {
                console.log("JOIN", "empresa_" + punto.id_empresa);
                sails.sockets.join(req, "empresa_" + punto.id_empresa);
                Punto_de_servicio.find({ id_empresa: punto.id_empresa }).exec(function (error, puntos) {
                    var num_puntos = puntos.length,
                        counter = 1;
                    var array_servicios = {};
                    for (var i in puntos) {
                        Servicios.find({ id_punto_de_servicio: puntos[i].id_punto_de_servicio }).exec(function (error, servicios) {
                            for (var i in servicios) {
                                array_servicios[servicios[i].id_servicio] = servicios[i];
                            }

                            if (counter < num_puntos) {
                                counter++;
                            } else {
                                res.json(Object.keys(array_servicios).map(function (key) { return array_servicios[key] }));
                                return false;
                            }
                        });
                    }
                });
            } else {
                console.log("JOIN", "partner_" + req.param('id'));
                sails.sockets.join(req, "partner_" + req.param('id'));
                Servicios.find({ id_punto_de_servicio: req.param('id') }).exec(function (error, servicios) {
                    if (error) {
                        res.negotiate(error); console.log("el error es", error);
                        return false;
                    } else {
                        res.json(servicios);
                        return false;
                    }
                });
            }
        });
    },



    aceptarservicio: async function (req, res) {

        console.log('aceptando servicio');
        try {

            var dat = new Date();
            //res.negotiate(error); console.log("el error es",error);
            var servicios = await Servicios.update({ id_servicio: req.param('id') }, { estado: true, status: 'Aceptado' });
            servicio = servicios[0];
            servicio.actualizado = dat.toISOString().slice(0, 19).replace('T', ' ');

            var usuario = await Usuario.findOne({ id_usuario: servicio.id_usuario });
            var punto = await Punto_de_servicio.findOne({ id_punto_de_servicio: servicio.id_punto_de_servicio });

            if (punto.id_empresa)
                sails.sockets.broadcast("empresa_" + punto.id_empresa, servicio);

            sails.sockets.broadcast("partner_" + servicio.id_punto_de_servicio, servicio);
            sails.sockets.broadcast("usuario_" + servicio.id_usuario, "updateservice", servicio);

            PushService.send_notification_push(usuario, 'partner_respuesta_si', { servicio: servicio });
            MailService.enviarCorreoAceptacionServicio({ usuario: usuario, servicio: servicio });

            //responder la peticion 
            servicio.messageid = req.param('messageid') || null;
            return res.ok(servicio);
        }
        //en dado caso de cualquier error, lo manejamos aca
        catch (error) {
            console.log('----> Error capturado <-----');
            error.messageid = req.param('messageid') || null;
            return res.serverError(error);
        }
    },

    rechazarservicio: async function (req, res) {

        console.log('rechazandoservicio');

        try {

            var servicio = await Servicios.findOne({ id_servicio: req.param('id') });
            servicio.actualizado = new Date();
            servicio.quien = "partner";
            servicio.cancelado = true;

            var cancelado = await Cancelados.findOrCreate({ id_servicio: servicio.id_servicio }, servicio);

            var updated = await Servicios.update({ id_servicio: req.param('id') }, {
                status: 'Rechazado',
                estado: false,
                terminado: true
            });

            var usuario = await Usuario.findOne({ id_usuario: servicio.id_usuario });
            var punto = await Punto_de_servicio.findOne({ id_punto_de_servicio: servicio.id_punto_de_servicio });

            if (punto.id_empresa)
                sails.sockets.broadcast("empresa_" + punto.id_empresa, servicio);

            sails.sockets.broadcast("partner_" + servicio.id_punto_de_servicio, servicio);
            
            sails.sockets.broadcast("usuario_" + servicio.id_usuario, "updateservice", servicio);

            PushService.send_notification_push(usuario, 'partner_respuesta_no', { servicio: servicio });
            MailService.enviarCorreoRechazoServicio({ usuario: usuario, servicio: servicio });

            //responder la peticion 
            servicio.messageid = req.param('messageid') || null;
            return res.ok(servicio);
        }
        //en dado caso de cualquier error, lo manejamos aca
        catch (error) {
            console.log('----> Error capturado <-----');
            error.messageid = req.param('messageid') || null;
            return res.serverError(error);
        }
    },

    //@DEPRECATED metodo para mantener la compatibilidad con la anterior version de la aplicacion partner
    //cuando las apps se suban a ios quitar este metodo
    respuesta: async function (req, res) {
        if (req.param('respuesta')) {
            try {

                var dat = new Date();
                //res.negotiate(error); console.log("el error es",error);
                var servicios = await Servicios.update({ id_servicio: req.param('id') }, { estado: true, status: 'Aceptado' });
                servicio = servicios[0];
                servicio.actualizado = dat.toISOString().slice(0, 19).replace('T', ' ');

                var usuario = await Usuario.findOne({ id_usuario: servicio.id_usuario });
                var punto = await Punto_de_servicio.findOne({ id_punto_de_servicio: servicio.id_punto_de_servicio });

                if (punto.id_empresa)
                    sails.sockets.broadcast("empresa_" + punto.id_empresa, servicio);

                sails.sockets.broadcast("partner_" + servicio.id_punto_de_servicio, servicio);
                sails.sockets.broadcast("usuario_" + servicio.id_usuario, "updateservice", servicio);

                PushService.send_notification_push(usuario, 'partner_respuesta_si', { servicio: servicio });
                MailService.enviarCorreoAceptacionServicio({ usuario: usuario, servicio: servicio });

                //responder la peticion 
                servicio.messageid = req.param('messageid') || null;
                return res.ok(servicio);
            }
            //en dado caso de cualquier error, lo manejamos aca
            catch (error) {
                console.log('----> Error capturado <-----');
                error.messageid = req.param('messageid') || null;
                return res.serverError(error);
            }
        } else {
            console.log('Cancelando');
            try {

                var servicio = await Servicios.findOne({ id_servicio: req.param('id') });
                servicio.actualizado = new Date();
                servicio.quien = "partner";
                servicio.cancelado = true;

                var cancelado = await Cancelados.findOrCreate({ id_servicio: servicio.id_servicio }, servicio);

                var updated = await Servicios.update({ id_servicio: req.param('id') }, {
                    status: 'Rechazado',
                    estado: false,
                    terminado: true
                });

                var usuario = await Usuario.findOne({ id_usuario: servicio.id_usuario });
                var punto = await Punto_de_servicio.findOne({ id_punto_de_servicio: servicio.id_punto_de_servicio });

                if (punto.id_empresa)
                    sails.sockets.broadcast("empresa_" + punto.id_empresa, servicio);

                sails.sockets.broadcast("partner_" + servicio.id_punto_de_servicio, servicio);
                //sails.sockets.broadcast("usuario_" + servicio.id_usuario, "updateservice", servicio);

                PushService.send_notification_push(usuario, 'partner_respuesta_no', { servicio: servicio });
                MailService.enviarCorreoAceptacionServicio({ usuario: usuario, servicio: servicio });

                //responder la peticion 
                servicio.messageid = req.param('messageid') || null;
                return res.ok(servicio);
            }
            //en dado caso de cualquier error, lo manejamos aca
            catch (error) {
                console.log('----> Error capturado <-----');
                error.messageid = req.param('messageid') || null;
                return res.serverError(error);
            }
        }

    },
    //Actualiza a la base de datos que el servicio ya fue terminado
    terminar: function (req, res) {
        Servicios.update({ id_servicio: req.param('id') }, { terminado: true, status: 'Terminado' }).exec(function (error, servicio) {
            if (error) {
                res.negotiate(error); console.log("el error es", error);
                return false;
            }

            servicio = servicio[0];

            Punto_de_servicio.findOne({ id_punto_de_servicio: servicio.id_punto_de_servicio }).exec(function (error, punto) {
                if (error) {
                    res.negotiate(error); console.log("el error es", error);
                    return false;
                }

                Usuario.findOne({ id_usuario: servicio.id_usuario }).exec(function (error, usuario) {
                    if (error) {
                        res.negotiate(error); console.log("el error es", error);
                        return false;
                    }

                    PushService.send_notification_push(usuario, 'partner_terminado', { usuario: usuario, servicio: servicio });

                    sails.sockets.broadcast("usuario_" + usuario.id_usuario, {
                        id_punto_de_servicio: servicio.id_punto_de_servicio,
                        nombre_punto_de_servicio: servicio.nombre_punto_de_servicio,
                        imagen: punto.imagen,
                        nombre_servicio: servicio.nombre_servicio,
                        fecha: servicio.fecha,
                        precio: servicio.precio
                    });

                    if (punto.id_empresa)
                        sails.sockets.broadcast("empresa_" + punto.id_empresa, servicio);

                    sails.sockets.broadcast("partner_" + servicio.id_punto_de_servicio, servicio);

                    return res.ok();
                });
            });
        });
    },
    //Registra el token del usuario cuando se loggea
    registrartoken: function (req, res) {
        Punto_de_servicio.findOne({ id_punto_de_servicio: req.param('id') }).exec(function (error, punto) {
            if (error) {
                res.negotiate(error); console.log("el error es", error);
                return false;
            }

            if (punto.es_master) {
                sails.log.debug("PARTNER", [punto.id_punto_de_servicio, punto.id_empresa, punto.nombre]);
                Empresa.findOne({ id_empresa: punto.id_empresa }).exec(function (error, empresa) {
                    empresa.dispositivos = empresa.dispositivos || [];
                    empresa.dispositivos.push({ token: req.param('token'), plataforma: req.param('plataforma') });
                    sails.log.debug("PARTNER", { token: req.param('token'), plataforma: req.param('plataforma') });
                    Empresa.update({ id_empresa: punto.id_empresa }, { dispositivos: empresa.dispositivos }).exec(function (error, empresa) {
                        if (error) {
                            res.negotiate(error); console.log("el error es", error);
                            return false;
                        } else {
                            res.json({});
                            return false;
                        }
                    });
                });
            } else {
                punto.dispositivos = punto.dispositivos || [];
                sails.log.debug("PARTNER", { token: req.param('token'), plataforma: req.param('plataforma') });
                punto.dispositivos.push({ token: req.param('token'), plataforma: req.param('plataforma') });
                Punto_de_servicio.update({ id_punto_de_servicio: req.param('id') }, { dispositivos: punto.dispositivos }).exec(function (error, punto) {
                    if (error) {
                        res.negotiate(error); console.log("el error es", error);
                        return false;
                    } else {
                        return res.ok();
                    }
                });
            }
        });
    },

    //Elimina los tokens anteriores de la base de datos
    borrartoken: function (req, res) {
        Punto_de_servicio.findOne({ id_punto_de_servicio: req.param('id') }).exec(function (error, punto) {
            if (error) {
                res.negotiate(error); console.log("el error es", error);
                return false;
            }

            if (punto.es_master) {
                sails.sockets.leave(req, "empresa_" + punto.id_empresa);
                Empresa.findOne({ id_empresa: punto.id_empresa }).exec(function (error, empresa) {
                    empresa.dispositivos = empresa.dispositivos || [];
                    empresa.dispositivos = _.without(empresa.dispositivos, _.findWhere(empresa.dispositivos, { token: req.param('token') }));
                    Empresa.update({ id_empresa: punto.id_empresa }, { dispositivos: empresa.dispositivos }).exec(function (error, empresa) {
                        if (error) {
                            res.negotiate(error); console.log("el error es", error);
                            return false;
                        } else {
                            return res.ok();
                        }
                    });
                });
            } else {
                sails.sockets.leave(req, "partner_" + req.param('id'));
                punto.dispositivos = punto.dispositivos || [];
                punto.dispositivos = _.without(punto.dispositivos, _.findWhere(punto.dispositivos, { token: req.param('token') }));
                Punto_de_servicio.update({ id_punto_de_servicio: req.param('id') }, { dispositivos: punto.dispositivos }).exec(function (error, punto) {
                    if (error) {
                        res.negotiate(error); console.log("el error es", error);
                        return false;
                    } else {
                        return res.ok();
                    }
                });
            }
        });
    }
};
