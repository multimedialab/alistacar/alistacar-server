/**
 * UserController
 * @description :: Este controlador maneja toda la interacción entre usuario y servidor.
 * @field :: AIzaSyD9GSh15mDU7N3CDT4qgruwNW9jm3jxiL0   ---- 989819467752
 */

var request = require('request');
var path = require('path');
var fs = require('fs');
var moment = require('moment');
const exec = require('child_process').exec;
moment.locale('es');


//service.on("connected", function() { console.log("Connected"); }); service.on("transmitted", function(notification, device) { console.log("Notification transmitted to:" + device.token.toString("hex")); }); service.on("transmissionError", function(errCode, notification, device) { console.error("Notification caused error: " + errCode); }); service.on("timeout", function () { console.log("Connection Timeout"); }); service.on("disconnected", function() { console.log("Disconnected from APNS"); }); service.on("socketError", console.error);

//verifica los puntos mas cercanos segun su ubicacion
var getDistancia = function (lon1, lat1, lon2, lat2) {
    function toRad(number) {
        return number * Math.PI / 180;
    }

    var R = 6371; // metres
    var φ1 = toRad(lat1);
    var φ2 = toRad(lat2);
    var Δφ = toRad(lat2 - lat1);
    var Δλ = toRad(lon2 - lon1);

    var a = Math.sin(Δφ / 2) * Math.sin(Δφ / 2) + Math.cos(φ1) * Math.cos(φ2) * Math.sin(Δλ / 2) * Math.sin(Δλ / 2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

    return parseFloat((R * c).toFixed(1));
}
//verifica el horario del punto del servicio
var getEstado = function (horario) {
    var diaDeLaSemana = (moment().format('dddd') == "miércoles") ? "miercoles" : ((moment().format('dddd') == "sábado") ? "sabado" : moment().format('dddd'));

    if (horario[diaDeLaSemana])
        return moment().isBetween(moment(horario[diaDeLaSemana].hora_inicio, 'HH:mm'), moment(horario[diaDeLaSemana].hora_fin, 'HH:mm'));
    else
        return false;
}

var RANGO_BUSQUEDA = 10000;

module.exports = {

    testpush: function (req, res) {
        var descripcionparams = {
            "token": "token del dispositivo",
            "plataforma": "android,ios",
            "app": "cliente,partner"

        }
        //para hacer ppruebas
        var token = req.param('token');
        var plataforma = req.param('plataforma') ? req.param('plataforma') : "ios";
        var app = req.param('app') ? "cliente" : "partner";
        if (!token) {
            //return res.badRequest("Falta el parametro token");
            return res.send(
                {
                    "message": "Falta el parametro token",
                    "parametrosdoc": descripcionparams
                }
            );
        }
        var paramusados = {
            "token": token,
            "plataforma": plataforma,
            "app": app
        }
        PushService.send_push([{ "token": token, "plataforma": plataforma }], "pushtitle", "pushbody", "seccionapp", app);

        return res.send({
            "message": "Push enviada",
            "paramusados": paramusados,
            "parametrosdoc": descripcionparams
        });
    },
/* -- Enviar Push Notifications a todos los clientes -- */
    pushtoall: async function (req, res) {
        try{
        var body         = req.query;
        var usuarios     = await Usuario.find();
        var dispositivos = usuarios;

        var titulo  = body.title;
        var mensaje = body.content;
        var payload = null;
        var tipo_usuario = "cliente";

        return res.send(PushService.send_push(dispositivos, titulo, mensaje, payload, tipo_usuario));
        }catch(e){
            return res.serverError(e);
        }
    },

    login: function (req, res) {

        /* -- Busqueda del usuario en la base de datos(la contraseña está como texto plano en la tabla -- */
        Usuario.findOne({
            email: req.param('email').toLowerCase(),
            contrasena: req.param('password')
        }).exec(function (error, response) {
            if (error)
                return res.negotiate(error);
            else
                return res.json(response);
        });
    },

    registro: function (req, res) {

        /* -- Crear nuevo usuario en la base de datos -- */

        Usuario.findOne({
            email: req.param('email').toLowerCase()
        }).exec(function (error, response) {
            if (error)
                return res.negotiate(error);

            if (response != undefined || response != null) {
                return res.json(false);
            }
            var dat = new Date();
            Usuario.create({ nombre: req.param('nombre'), email: req.param('email').toLowerCase(), telefono: req.param('telefono'), contrasena: req.param('password'), created: dat.toISOString().slice(0, 19).replace('T', ' ') }).exec(function (error, response) {
                if (error)
                    return res.negotiate(error);
                else {

                    request({
                        url: 'http://admin.alistacar.com:3000/welcome-alistacar?to=' + encodeURIComponent(response.email) +
                            '&template=welcome-alistacar&tipo=Bienvenidos%20a%20Alistacar&parameters={"nombre":"' + response.nombre +
                            '","type_title":"Bienvenido(a) a Alistacar",' +
                            '"message_body":""}',
                        method: 'GET',
                        json: true,
                    },
                        function (error, response, body) {
                            if (error) {
                                return console.error('upload failed:', error);
                            }
                            console.log('Upload successful!  Server responded with:', body);
                        });
                    return res.json(response);
                }
            });

        });
    },

    password: function (req, res) {

        /* -- Mensaje de recordatorio de contraseña -- */

        Usuario.findOne({
            email: req.param('email').toLowerCase()
        }).exec(function (error, usuario) {
            if (error || !usuario)
                return res.negotiate(error);

            Mailer({
                from: '"Alistacar" <ecospautos@gmail.com>',
                to: usuario.email,
                subject: 'Alistacar, olvido de contraseña',
                template: 'La contraseña para ingresar es: <b>' + usuario.contrasena + '</b>'
            }, function (err, info) {
                if (error)
                    return res.negotiate(error);
                else
                    return res.ok(info);
            });
        });
    },

    facebook: function (req, res) {
        if (!req.isSocket) return res.badRequest();

        var busqueda = {
            email: req.param('email')
        };
        var datos = req.params.all();
        datos.nombre = req.param('name');
        datos.contrasena = req.param('email').split("@")[0] + req.param('telefono');

        Usuario.findOrCreate(busqueda, datos).exec(function (error, usuario) {
            if (error) {
                res.negotiate(error);
                return res.end();
            } else {
                res.json(usuario);
                return res.end();
            }
        });
    },

    xcalificar: function (req, res) {
        if (!req.isSocket) return res.badRequest();

        sails.sockets.join(req, "usuario_" + req.param('id'));
        Servicios.findOne({
            id_usuario: req.param('id'),
            estado: true,
            terminado: true,
            calificado: false
        }).exec(function (error, servicio) {
            if (error) return res.negotiate(error);
            if (!servicio) return res.notFound();
            Punto_de_servicio.findOne({
                id_punto_de_servicio: servicio.id_punto_de_servicio
            }).exec(function (error, punto) {
                if (error) return res.negotiate(error);
                //If not punto
                if (!punto) {
                    var punto = { imagen: null };
                }
                sails.sockets.broadcast("usuario_" + req.param('id'), {
                    id_servicio: servicio.id_servicio,
                    id_punto_de_servicio: servicio.id_punto_de_servicio,
                    nombre_punto_de_servicio: servicio.nombre_punto_de_servicio,
                    nombre_servicio: servicio.nombre_servicio,
                    fecha: servicio.fecha,
                    precio: servicio.precio,
                    imagen: punto.imagen
                });
                return res.ok();
            });
        });
    },

    ciudades: function (req, res) {
        Ciudad.find().exec(function (error, response) {
            if (error) {
                res.negotiate(error);
                return res.end();
            } else {
                res.json(response);
                return res.end();
            }

        });
    },
    // consigue las cordenas del dispositivo
    coordenadas: function (req, res) {
        var google_query = "https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyCkjRWgy2v43UTCFYDMCjgMuymDFphtJ-o&address=" + req.param('direccion').replace(/ /g, "+").replace('#', '');

        request(google_query, function (error, response, body) {
            if (error && response.statusCode != 200) {
                res.negotiate(error);
                return res.end();
            }

            var direccion = JSON.parse(body);
            res.json({
                latitude: direccion.results[0].geometry.location.lat,
                longitude: direccion.results[0].geometry.location.lng
            });
            return res.end();
        });
    },
    // envia las categorias para ser mostradas en la pantalla de inicio
    categorias: function (req, res) {
        Categoria.find({
            id_categoria_padre: null
        }).sort('posicion ASC').exec(function (error, response) {
            if (error) {
                res.negotiate(error);
                return res.end();
            } else {
                res.json(response);
                return res.end();
            }
        });
    },

    subcategorias: function (req, res) {
        Categoria.find({
            id_categoria_padre: req.param('id')
        }).exec(function (error, response) {
            if (error) {
                res.negotiate(error);
                return res.end();
            }

            res.json(_.sortBy(response, function (item) {
                return item.posicion
            }));
            return res.end();
        });
    },
    // Verifica las promociones existentes
    promociones: function (req, res) {
        Promociones.find().sort('posicion ASC').exec(function (error, response) {
            if (error) {
                res.negotiate(error);
                return res.end();
            } else {
                res.json(response);
                return res.end();
            }
        });
    },
    // muestra los puntos de servicio
    banners: function (req, res) {
        Banner.find().sort('posicion ASC').exec(function (error, response) {
            if (error) {
                res.negotiate(error);
                return res.end();
            } else {
                res.json(response);
                return res.end();
            }
        });
    },
    //Busca los puntos de servicio mas cercanos
    buscador: function (req, res) {
        var query = 'SELECT DISTINCT p.id_punto_de_servicio, p.imagen, p.nombre, p.latitud, p.longitud, p.slogan, p.horario FROM ecospautos.servicio s INNER JOIN ecospautos.punto_de_servicio p ON s.id_punto_de_servicio = p.id_punto_de_servicio WHERE s.nombre LIKE "%' + req.param('busqueda') + '%" OR s.descripcion LIKE "%' + req.param('busqueda') + '%" OR p.keywords LIKE "%' + req.param('busqueda') + '%";';
        Servicios.query(query, function (error, puntos) {
            if (error) {
                res.negotiate(error);
                return res.end();
            }

            _.each(puntos, function (punto, key) {
                punto.distancia = getDistancia(parseFloat(req.param('longitud')), parseFloat(req.param('latitud')), punto.longitud, punto.latitud);
                punto.abierto = getEstado(JSON.parse(punto.horario));
                delete punto.horario;
            });

            res.json(_.sortBy(_.filter(puntos, function (punto) {
                return punto.distancia < RANGO_BUSQUEDA
            }), 'distancia'));
            res.end();
        });
    },

    puntos: function (req, res) {
        var query = "SELECT DISTINCT p.id_punto_de_servicio, p.imagen, p.nombre, p.latitud, p.longitud, p.slogan, p.horario FROM ecospautos.servicio s  INNER JOIN ecospautos.punto_de_servicio p ON s.id_punto_de_servicio = p.id_punto_de_servicio WHERE s.id_categoria = " + req.param('categoria') + ";";

        Servicio.query(query, function (error, puntos) {
            if (error) {
                res.negotiate(error);
                return res.end();
            }

            _.each(puntos, function (punto, key) {
                punto.distancia = getDistancia(parseFloat(req.param('longitud')), parseFloat(req.param('latitud')), punto.longitud, punto.latitud);
                punto.abierto = getEstado(JSON.parse(punto.horario));
                delete punto.horario;
            });

            res.json(_.sortBy(_.filter(puntos, function (punto) {
                return punto.distancia < RANGO_BUSQUEDA
            }), 'distancia'));
            return res.end();
        });
    },

    puntosSubcategoria: function (req, res) {
        var query = "SELECT DISTINCT p.id_punto_de_servicio, p.imagen, p.nombre, p.latitud, p.longitud, p.slogan, p.horario FROM ecospautos.servicio s  INNER JOIN ecospautos.punto_de_servicio p ON s.id_punto_de_servicio = p.id_punto_de_servicio WHERE s.id_sub_categoria = " + req.param('categoria') + ";";

        Servicio.query(query, function (error, puntos) {
            if (error) {
                res.negotiate(error);
                return res.end();
            }

            _.each(puntos, function (punto, key) {
                punto.distancia = getDistancia(parseFloat(req.param('longitud')), parseFloat(req.param('latitud')), punto.longitud, punto.latitud);
                punto.abierto = getEstado(JSON.parse(punto.horario));
                delete punto.horario;
            });

            res.json(_.sortBy(_.filter(puntos, function (punto) {
                return punto.distancia < RANGO_BUSQUEDA
            }), 'distancia'));
            return res.end();
        });
    },

    punto: function (req, res) {
        Punto_de_servicio.findOne({
            id_punto_de_servicio: req.param('id')
        }).populate('servicios').populate('comentarios').exec(function (error, response) {
            if (error || !response) {
                res.negotiate(error);
                return res.end();
            }

            if (response.imagen == "") delete response.imagen;

            if (response.medio_pago) response.medio_pago = response.medio_pago.split(',');

            response.puntaje = (!response.puntaje) ? {
                votos: 0,
                votantes: 0
            } : {
                    votos: parseInt(response.puntaje.split(',')[0]),
                    votantes: parseInt(response.puntaje.split(',')[1])
                };
            response.horario = JSON.parse(response.horario);
            response.abierto = getEstado(response.horario);

            res.json(response);
            return res.end();

        });
    },
    revisar: function (req, res) {

        /* -- El administrador marca el servicio como revisado 
            para no necesitarlo en la lista de pendientes -- */

        console.log('-> Marking serice as admin-checked <-');
        Servicios.update({
            fecha: req.param('fc')
        }, {
                revisado: true
            }).exec(function (error, updated) {
                if (error) {
                    res.negotiate(error);
                    return res.end();
                }
                res.json({ succes: true });
            })
    },

    /**
     * Nuevo servicio solicitado
     * @params vienen dentro del body de la peticion: usuario, servicio,id_usuario, fecha
     */
    solicitar: async function (req, res) {

        console.log('----> New service asked <-----', req.param('servicio'));
        try {

            var usuario = await Usuario.findOne({ id_usuario: req.param('usuario') })
            var servicio = await Servicio.findOne({ id_servicio: req.param('servicio') })
            var punto = await Punto_de_servicio.findOne({
                id_punto_de_servicio: servicio.id_punto_de_servicio
            }).populate("id_ciudad")

            var solicitud = await Servicios.create({
                id_punto_de_servicio: servicio.id_punto_de_servicio,
                nombre_punto_de_servicio: punto.nombre,
                id_empresa: (punto.es_master) ? punto.id_empresa : null,
                ciudad: punto.id_ciudad.nombre,
                id_usuario: req.param('id_usuario'),
                nombre_servicio: servicio.nombre, fecha: new Date(req.param('fecha')),
                nombre_cliente: usuario.nombre, telefono_cliente: usuario.telefono,
                precio: servicio.precio, descuento: servicio.descuento,
                estado: false, terminado: false, status: 'Solicitado'
            })

            NotificarService.notificarParnerDelServicio(punto, solicitud, req);
            if (punto.id_empresa) {
                NotificarService.notificarParnerMasterDelServicio(punto, solicitud)
            }

            MailService.enviarCorreoSolicitudServicio({ usuario: usuario, solicitud: solicitud });

            //responder la peticion 
            solicitud.messageid = req.param('messageid') || null;
            return res.ok(solicitud);

        }
        //en dado caso de cualquier error, lo manejamos aca
        catch (error) {
            console.log('----> Error capturado <-----');
            error.messageid = req.param('messageid') || null;
            return res.serverError(error);
        }

    },
    // cambia  el estado del servicio en la base datos
    cancelar: async function (req, res) {

        try {

            var servicio = await Servicios.findOne({ id_servicio: req.param('id') });
            servicio.quien = "usuario";
            servicio.cancelado = true;


            var updated = await Servicios.update({ id_servicio: req.param('id') }, {
                status: 'Cancelado',
                estado: false,
                terminado: true
            });
            var cancelado = await Cancelados.findOrCreate({ id_servicio: servicio.id_servicio }, servicio);

            //var cancelado = await Cancelados.create(servicio);

            var punto = await Punto_de_servicio.findOne({
                id_punto_de_servicio: servicio.id_punto_de_servicio
            });


            if (punto.id_empresas) {
                sails.sockets.broadcast("empresa_" + servicio.id_empresa, servicio);
                Empresa.findOne({
                    id_empresa: punto.id_empresa
                }).exec(function (error, empresa) {

                    PushService.send_notification_push(empresa.dispositivos, 'usuario_cancelar_servicio');
                });
            }

            sails.sockets.broadcast("partner_" + servicio.id_punto_de_servicio, servicio);
            PushService.send_notification_push(punto.dispositivos, 'usuario_cancelar_servicio');
            var solicitud = {};
            //responder la peticion 
            solicitud.messageid = req.param('messageid') || "";
            return res.ok(solicitud);
        }
        catch (error) {
            console.log('----> Error capturado <-----');
            error.messageid = req.param('messageid')
            return res.serverError(error);
        }



    },

    estado: function (req, res) {
        Servicios.findOne({
            id_servicio: req.param('id')
        }).exec(function (error, servicio) {
            if (error) {
                res.negotiate(error);
                return res.end();
            } else {
                res.json(servicio);
                return res.end();
            }
        });
    },

    calificacion: function (req, res) {
        Servicios.update({
            id_servicio: req.param('id_servicio')
        }, {
                calificado: true,
                status: 'Calificado'
            }).exec(function (error, servicio) {
                if (error) {
                    res.negotiate(error);
                    return res.end();
                }

                Comentarios.create({
                    id_punto_de_servicio: req.param('id_punto_de_servicio'),
                    contenido: req.param('contenido'),
                    calificacion: req.param('calificacion')
                }).exec(function (error, comentario) {
                    if (error) {
                        res.negotiate(error);
                        return res.end();
                    }
                    return res.ok();
                });
            });
    },

    servicios: function (req, res) {
        Servicios.find({
            id_usuario: req.param('id'),
            terminado: false
        }).sort('fecha ASC')
            .exec(function (error, servicios) {
                if (error) {
                    res.negotiate(error);
                    return res.end();
                }
                sails.sockets.join(req, "usuario_" + req.param('id'));
                res.json(servicios);
                return res.end();
            });
    },

    todoservicios: function (req, res) {
        Servicios.find().exec(function (error, response) {
            if (error) {
                res.negotiate(error);
                return res.end();
            } else {
                res.jsonp(response);
                return res.end();
            }
        });
    },

    pendientes: function (req, res) {
        Servicios.find({
            id_usuario: req.param('id'),
            estado: false,
            terminado: false
        }).exec(function (error, servicios) {
            if (error) {
                res.negotiate(error);
                return res.end();
            } else {
                res.json(servicios);
                return res.end();
            }
        });
    },
    // Retorna todos los servicios solicitados de la base de datos
    historico: function (req, res) {
        Servicios.find({
            id_usuario: req.param('id'),
            estado: true,
            terminado: true
        }).exec(function (error, servicios) {
            if (error) {
                res.negotiate(error);
                return res.end();
                console.log('esto es laaaa error', res.end());
            } else {
                res.json(servicios);
                return res.end();
                console.log('esto es laaaa', res.end());
            }
        });
    },
    // actualiza la informacion del usuario en la base de datos
    actualizacion: function (req, res) {
        Usuario.update({
            id_usuario: req.param('id')
        }, {
                nombre: req.param('nombre'),
                email: req.param('email'),
                telefono: req.param('telefono')
            }).exec(function (error, updated) {
                if (error) {
                    res.negotiate(error);
                    return res.end();
                } else {
                    return res.ok();
                }
            });
    },

    contrasena: function (req, res) {
        Usuario.update({
            id_usuario: req.param('id')
        }, {
                contrasena: req.param('contrasena')
            }).exec(function (error, updated) {
                if (error) {
                    res.negotiate(error);
                    return res.end();
                } else {
                    return res.ok();
                }
            });
    },
    // Registra el token cuando se logea el usuario en el dispositivo
    registrarToken: function (req, res) {
        console.log('+++++++++++token', req.param('token'), req.param('dispositivo'))
        Usuario.update({
            id_usuario: req.param('id')
        }, {

                token: req.param('token'),
                dispositivo: req.param('dispositivo')
            }).exec(function (error, updated) {
                if (error) {
                    res.negotiate(error);
                    return res.end();
                } else {
                    return res.ok();
                }
            });
    },

    restart: function (req, res) {
        exec('pm2 restart app', (error, stdout, stderr) => {
            if (error) {
                console.error(`exec error: ${error}`);
                return;
            }
            console.log(`stdout: ${stdout}`);
        });
    }

};
