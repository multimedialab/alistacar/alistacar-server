var NotificarService = {

    notificarParnerDelServicio: function (punto, solicitud, req) {
        //notificamos via sockets
        sails.sockets.join(req, "usuario_" + req.param('id_usuario'));
        sails.sockets.broadcast("partner_" + solicitud.id_punto_de_servicio, solicitud);

        PushService.send_notification_push(punto.dispositivos, 'usuario_solicitud_servicio');
        return true;
    },

    notificarParnerMasterDelServicio: function (punto, solicitud) {
        if (!punto.id_empresa) return;

        sails.sockets.broadcast("empresa_" + punto.id_empresa, solicitud);
        Empresa.findOne({
            id_empresa: punto.id_empresa
        }).exec(function (error, empresa) {
            PushService.send_notification_push(empresa.dispositivos, 'usuario_solicitud_servicio');
        });
    }
};


module.exports = NotificarService;  