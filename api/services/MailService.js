var request = require('request');
var moment = require('moment');
var EmailTemplate = require('email-templates').EmailTemplate;
var MailService = {

    enviarCorreoSolicitudServicio: function (options, done) {
        //var templateDir = path.join(__dirname, '../../assets/templates', 'email-template');
        //var template = new EmailTemplate(templateDir);
        var usuario = options.usuario;
        var solicitud = options.solicitud;
        request({
            url: 'http://admin.alistacar.com:3000/alistacar?to=' + usuario.email +
                '&template=alistacar-mail&tipo=solicitud%20de%20servicio&parameters={"nombre_servicio":"' + solicitud.nombre_servicio +
                '","dia":"' + moment(solicitud.fecha).format("DD-MM-YYYY") +
                '","hora":"' + moment(solicitud.fecha).format("HH:mm") +
                '","type_title":"Has solicitado el servicio","message_body":"Tu solicitud está siendo revisada, seras notificado de la respuesta por la aplicación y mediante un email.","general_type":"Solicitud de Servicio"}',
            method: 'GET',
            json: true,
        },
            function (error, response, body) {
                if (error) {
                    console.error('Fallo el envio de correo, enviarCorreoSolicitudServicio', error);
                } else {
                    console.log('Envio de correo enviarCorreoSolicitudServicio correcto');
                }
            });

        return true;
    },

    enviarCorreoAceptacionServicio: function (options, done) {

        var usuario = options.usuario;
        var servicio = options.servicio;

        request({
            url: 'http://admin.alistacar.com:3000/alistacar/partner?to=' + usuario.email +
                '&template=alistacar-mail&tipo=respuesta%20de%20servicio&parameters={"nombre_servicio":"' + servicio.nombre_punto_de_servicio +
                '","dia":"' + moment(servicio.fecha).format("DD-MM-YYYY") +
                '","hora":"' + moment(servicio.fecha).format("HH:mm") +
                '","type_title":"Han aceptado el servicio","message_body":"Tu solicitud ha sido aceptada. No olvides calificar cuando te presten el servicio.","general_type":"Respuesta de Servicio"}',
            method: 'GET',
            json: true,
        },
            function (error, response, body) {
                if (error) {
                    console.error('Fallo el envio de correo, enviarCorreoAceptacionServicio', error);
                } else {
                    console.log('Envio de correo enviarCorreoSolicitudServicio correcto');
                }
            });
    },
    enviarCorreoRechazoServicio: function (options, done) {

        var usuario = options.usuario;
        var servicio = options.servicio;
        request({
            url: 'http://admin.alistacar.com:3000/alistacar/partner?to=' + usuario.email +
                '&template=alistacar-mail&tipo=respuesta%20de%20servicio&parameters={"nombre_servicio":"' + servicio.nombre_servicio +
                '","dia":"' + moment(servicio.fecha).format("DD-MM-YYYY") +
                '","hora":"' + moment(servicio.fecha).format("HH:mm") +
                '","type_title":"Han rechazado el servicio","message_body":"Tu solicitud ha sido rechazada. Intenta en una hora o día diferente.","general_type":"Respuesta de Servicio"}',
            method: 'GET',
            json: true,
        },
            function (error, response, body) {
                if (error) {
                    console.error('Fallo el envio de correo, enviarCorreoAceptacionServicio', error);
                } else {
                    console.log('Envio de correo enviarCorreoRechazoServicio correcto');
                }
            });

    }

};


module.exports = MailService;  