var gcm = require('fcm-node');
var apn = require('apn');
var substitute = require('token-substitute');

var partner_android_sender = new gcm('AAAAXd_3eGY:APA91bFtEAroh5Ws9mDgPTPo-AYE0J4ZdE5GE8NokVtltdMCNJjWL2NPQ6dKo5ZjozeqpsDDq-U4zEl5BhoMsYi6k3J-sHbuN_suZEeQhFrOipGIFoPy2uE4xnhVbjZeG9M2-dF6zCwA');
var cliente_android_sender = new gcm('AAAA5nXWd-g:APA91bGdUZJyEMYwEq9D6k0iBu0xoVy1BeC41ady7ZoHzBx8DYiaK5gd8d4fmxSZTm1-peZsFiZ6KQOBSiI-R3nB0Myoq0j8_709Bxxq6_hgEPmOEa8ds1DucBXgRnFcuNa7N7k7gy2s');


var ConnectData = {
	  token: {
		      key: "assets/certificates/user/prod/AuthKey_ZSHE6W9QT6.p8",
		      keyId: "ZSHE6W9QT6",
		      teamId: "54MALLVDA8"
		    },
	 production: true
};
var partner_ios_sender = new apn.Provider(ConnectData);
var cliente_ios_sender = new apn.Provider(ConnectData);


var push_mensajes = {
  //Se pueden colocar tokens para ser reemplazados
  //usando el modulo https://www.npmjs.com/package/token-substitute
  'usuario_solicitud_servicio': {
    titulo: "Solicitud de servicio",
    mensaje: "Un usuario ha solicitado un servicio",
    payload: "servicios.solicitudes",
    tipo_usuario: "partner"
  },
  'usuario_cancelar_servicio': {
    titulo: "Cancelación de servicio",
    mensaje: "Un usuario ha cancelado un servicio",
    payload: "servicios.solicitudes",
    tipo_usuario: "partner"
  },
  "partner_respuesta_si": {
    titulo: '#{servicio.nombre_punto_de_servicio}',
    mensaje: "Tu solicitud del servicio #{servicio.nombre_servicio} ha sido aceptada.",
    payload: null,
    tipo_usuario: "cliente"
  },
  "partner_respuesta_no": {
    titulo: '#{servicio.nombre_punto_de_servicio}',
    mensaje: "El punto de servicio no puede atender tu solicitud #{servicio.nombre_servicio} Intenta en una hora o día diferente.",
    payload: null,
    tipo_usuario: "cliente"
  },
  "partner_terminado": {
    titulo: '#{servicio.nombre_punto_de_servicio}',
    mensaje: "El servicio #{servicio.nombre_servicio} ahora se está prestando, recuerda puntuar el punto de servicio cuando este termine.",
    payload: null,
    tipo_usuario: "cliente"
  }

}


var pushprivate = {
  send_push_android: function (dispositivos, titulo, mensaje, payload, tipo_usuario) {
    console.log("--->ENVIANDO ANDROID")
    var actual_time = new Date();
    
    var message = {
        registration_ids:dispositivos,
        notification:{
          title: titulo,
          body: mensaje + actual_time.getDate() +"/"+ actual_time.getMonth() + "/" + actual_time.getFullYear()+"-"+ actual_time.getHours() +":"+ actual_time.getMinutes()
      }
    }

    tipo_usuario = tipo_usuario || "cliente";
    var android_sender = (tipo_usuario == "cliente") ? cliente_android_sender : partner_android_sender;

    android_sender.send(message, function (err, response) {
      if (err) {
        console.error("errorpush",err);
      } else {
        console.log("funcionopush",response);
      }
    });
  },

  send_push_ios: function (dispositivos, mensaje, payload, tipo_usuario) {
    if (dispositivos.length < 1)
      return;

    tipo_usuario       = tipo_usuario || "cliente";
    var ios_sender     = (tipo_usuario == "cliente") ? cliente_ios_sender : partner_ios_sender;	
    var topic_appid    = (tipo_usuario == "cliente") ? "com.mocion.alistacar" : "com.mocionsoft.alistacarpartner";	
    
    var note = new apn.Notification();

    note.topic = topic_appid;    

    note.badge = 1;
    note.contentAvailable = 1;
    note.alert = mensaje;
    note.sound = 'chime.caf';
    note.payload = { payload: payload };
    

    console.log("--->>>>>> enviando push a los siguiente dispositivos",dispositivos);
    ios_sender.send(note, dispositivos).then(function (result) {
      console.log("sent:", result.sent.length);
      console.log("failed:", result.failed.length);
      console.log(result.failed);
    });
 } 

}

var send_notification_push = function (dispositivos, accion, variables) {
  console.log(accion, dispositivos)
  if (!push_mensajes[accion])
    return;

  //Se pueden colocar tokens para ser reemplazados
  //usando el modulo https://www.npmjs.com/package/token-substitute
  var message = push_mensajes[accion];
  message = substitute(message, { tokens: variables });
  console.log(message)
  send_push(dispositivos, message.titulo, message.mensaje, message.payload, message.tipo_usuario)

}

var send_push = function (dispositivos, titulo, mensaje, payload, tipo_usuario) {

  if (!dispositivos)
    return;

  // por si se pasa un solo dispositivo
  if (!Array.isArray(dispositivos)) {
    dispositivos = [dispositivos];
  }

  var titulo = titulo || null;
  var mensaje = mensaje || null;
  var payload = payload || null;

  //los separamos por plataforma
  var devices = {
    android: [],
    ios: []
  };

  for (var i in dispositivos) {

    if (!dispositivos[i].token) {
      continue;
    }
    //miramos si tenemos tokens repetidos para que no se envien las push multiples veces
    if (devices.android.indexOf(dispositivos[i].token) !== -1) {
      continue;
    }
    if (devices.ios.indexOf(dispositivos[i].token) !== -1) {
      continue;
    }

    if (dispositivos[i].plataforma) {
      devices[dispositivos[i].plataforma].push(dispositivos[i].token);

    } else if (dispositivos[i].dispositivo) {
      devices[dispositivos[i].dispositivo].push(dispositivos[i].token);
    }
  }

  
  console.log('push: ', "titulo",titulo, "tipousuario",tipo_usuario,devices)
  
  if (devices.android.length)
    pushprivate.send_push_android(devices.android, titulo, mensaje, payload, tipo_usuario);

  if (devices.ios.length)
    pushprivate.send_push_ios(devices.ios, titulo + ": " + mensaje, payload, tipo_usuario);

}

var PushService = {
  send_notification_push: send_notification_push,
  send_push: send_push
}


module.exports = PushService;


