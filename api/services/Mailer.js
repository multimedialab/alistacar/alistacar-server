var nodemailer = require('nodemailer');
var transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: 'apps@mocionsoft.com',
        pass: 'mocion2040'
    }
});

module.exports = function(options, done) {
    transporter.sendMail({
        from: options.from,
        to: options.to,
        subject: options.subject,
        html: options.template
    }, done);
}