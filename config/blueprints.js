/**
 * http://sailsjs.org/documentation/reference/configuration/sails-config-blueprints
 */
module.exports.blueprints = {

  actions: false,
  
  rest: false,
  
  shortcuts: false,
  
  populate: false

};
