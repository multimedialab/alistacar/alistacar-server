/**
 * http://sailsjs.org/documentation/reference/configuration/sails-config-routes
 */
module.exports.routes = {

    /**
     * Las siguientes rutas corresponden a peticiones hechas desde la aplicación del usuario.
     */

    "GET /cliente/testpush": "UserController.testpush",

    "GET /restart": "UserController.restart",

    "GET /user/pushtoall/": "UserController.pushtoall",

    "POST /login-user": "UserController.login",

    "POST /registro": "UserController.registro",

    "POST /facebook": "UserController.facebook",

    "POST /olvidopassword": "UserController.password",

    "GET /xcalificar/:id": "UserController.xcalificar",

    "GET /revisar": "UserController.revisar",

    "GET /ciudades": "UserController.ciudades",

    "GET /coordenadas/:direccion": "UserController.coordenadas",

    "GET /categorias": "UserController.categorias",

    "GET /sub-categorias/:id": "UserController.subcategorias",

    "GET /promociones": "UserController.promociones",

    "GET /banners": "UserController.banners",

    "GET /buscador/:busqueda/:latitud/:longitud": "UserController.buscador",

    "GET /puntos/:categoria/:latitud/:longitud": "UserController.puntos",

    "GET /puntossubcategoria/:categoria/:latitud/:longitud": "UserController.puntosSubcategoria",

    "GET /punto/:id": "UserController.punto",

    "POST /solictiar": "UserController.solicitar",

    "POST /cancelar": "UserController.cancelar",

    "GET /estado/:id": "UserController.estado",

    "GET /servicios-usuario/:id": "UserController.servicios",

    "GET /todoservicios": "UserController.todoservicios",

    "GET /servicios-pendientes/:id": "UserController.pendientes",

    "POST /calificacion": "UserController.calificacion",

    "GET /historico/:id": "UserController.historico",

    "POST /actualizar-datos": "UserController.actualizacion",

    "POST /actualizar-contrasena": "UserController.contrasena",

    "POST /registrar-token": "UserController.registrarToken",

    /**
     * La siguientes rutas corresponden a peticiones hechas desde la aplicación del partner.
     */

    "POST /partner/login-partner": "PartnerController.login",

    "GET /partner/servicios/:id": "PartnerController.servicios",

    "GET /partner/pushtoall/": "PartnerController.pushtoall",

    "POST /partner/aceptarservicio": "PartnerController.aceptarservicio",

    "POST /partner/rechazarservicio": "PartnerController.rechazarservicio",

    "POST /partner/terminar": "PartnerController.terminar",

    "POST /partner/registrar-token": "PartnerController.registrartoken",

    "POST /partner/borrar-token": "PartnerController.borrartoken"

};