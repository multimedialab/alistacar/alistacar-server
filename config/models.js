/**
 * http://sailsjs.org/documentation/reference/configuration/sails-config-models
 */
module.exports.models = {

  connection: 'mysql',
  
  migrate: 'safe'

};
