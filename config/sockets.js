/**
 * http://sailsjs.org/#!/documentation/reference/sails.config/sails.config.sockets.html
 */

module.exports.sockets = {
  
  adapter: 'memory',
  
  transports: [
    "polling",
    "websocket"
  ],

  cookie:false,

  pingInteral:20000,
  grant3rdPartyCookie: true

};
