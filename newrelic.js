'use strict'

/**
 * New Relic agent configuration.
 *
 * See lib/config.default.js in the agent distribution for a more complete
 * description of configuration variables and their potential values.
 */
exports.config = {
  /**
   * Array of application names.
   */
  app_name: ['Alistacar - NewRelic'],
  /**
   * Your New Relic license key.
   */
  license_key: '677aec4700432f7d3b96a6dd7eb34537e3acf96d',
  logging: {
    /**
     * Level at which to log. 'trace' is most useful to New Relic when diagnosing
     * issues with the agent, 'info' and higher will impose the least overhead on
     * production applications.
     */
    level : 'warn',
    rules: { 
      ignore: ['^/socket.io/*/xhr-polling']
    }
  }
}
